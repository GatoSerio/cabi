$(function () {

    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $('#nav').addClass('min');
        } else {
            $('#nav').removeClass('min');
        }
    })
	
		/*Slideshow init*/
		$('.carousel.carousel-slider').carousel({
			fullWidth: true,
			indicators: true
		}, setTimeout(autoplay, 21000)).css('height', '100vh');

 function autoplay() {
   $('.carousel').carousel('next');
   setTimeout(autoplay, 4500);
 }

    /*SVG FALLBACK*/
    function supportsSvg() {
        return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1");
    }
    if (!supportsSvg()) {
        var imgs = document.getElementsByTagName('img');
        var svgExtension = /.*\.svg$/
        var l = imgs.length;
        for (var i = 0; i < l; i++) {
            if (imgs[i].src.match(svgExtension)) {
                imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
                console.log(imgs[i].src);
            }
        }
    }

    /*SMOOTH SCROLL*/
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        }
                    });
                }
            }
        });

    /*CONSOLE LOG OUTPUTS - printing debug and nice to know info*/
    console.log("SVG Support result: " + supportsSvg())
})
